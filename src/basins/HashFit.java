package basins;

import tsp.Solution;
import utils.Similarity;

public class HashFit{
	private long hash;
	private int fitness;
	private int depth;
	
	public HashFit(long hash, int fitness, int depth) {
		this.hash = hash;
		this.fitness = fitness;
		this.depth = depth;
	}
	
	public HashFit(long hash, int fitness) {
		this.hash = hash;
		this.fitness = fitness;
		this.depth = 0;
	}
	
	public HashFit(Solution solution) {
		this.hash = solution.getHash();
		this.fitness = solution.getFitness();
		this.depth = 0;
	}
	
	public HashFit(Solution solution, int depth) {
		this.hash = solution.getHash();
		this.fitness = solution.getFitness();
		this.depth = depth;
	}
	
	public int getDepth() {
		return depth;
	}
	
	public void setDepth(int depth) {
		this.depth = depth;
	}
	
	public long getHash() {
		return hash;
	}
	
	public void setHash(long hash) {
		this.hash = hash;
	}
	
	public int getFitness() {
		return fitness;
	}
	
	public void setFitness(int fitness) {
		this.fitness = fitness;
	}
	
	/*public boolean equals(HashFit hashFit) {
		return (this.getFitness() == hashFit.getFitness()) && (this.getHash() == hashFit.getHash());
	}*/
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		HashFit hashFit = (HashFit) o;
		return (this.getFitness() == hashFit.getFitness()) && (this.getHash() == hashFit.getHash());
	}
	
	@Override
	public String toString() {
		return "HashFit{" +
				"hash=" + hash +
				", fitness=" + fitness +
				", depth=" + depth +
				'}';
	}
}
