package basins;

import tsp.Instance;
import tsp.Solution;

import java.io.File;
import java.util.ArrayList;

public interface BasinsOfAttraction {
	
	File buildBasin(Instance tsp, Solution solution);
	
	int sizeBasin(File basin);
	
	int minDepth(File basin);
	
	int maxDepth(File basin);
	
	float avgDepth(File basin);
	
	void checkAround(Instance tsp, Solution solution, ArrayList<HashFit> basin, ArrayList<HashFit> border, int depth);
}
