package basins;

import tsp.Hash;
import tsp.Instance;
import tsp.Solution;
import utils.Movement;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;

public class TwoOpt implements BasinsOfAttraction {
	@Override
	public File buildBasin(Instance tsp, Solution solution) {
		new localsearch.TwoOpt().bestFit(solution,tsp);
		long hash = solution.getHash();
		int fitness = solution.getFitness();
		//new file with "{hash}-{fitness}" as name
		int depth = 0;
		ArrayList<HashFit> basin = new ArrayList<>(), border = new ArrayList<>();
		basin.add(new HashFit(hash,fitness,depth));
		System.out.println(basin.get(0));
		checkAround(tsp,solution,basin,border,++depth);
		//basin = explore(tsp,solution);
		System.out.println(basin.size());
		System.out.println(border.size());
		/*for(HashFit hf : basin)
			System.out.println(hf.getHash()+";"+hf.getFitness()+";"+hf.getDepth());
		for(HashFit hf : border)
			System.out.println(hf.getHash()+";"+hf.getFitness()+";"+hf.getDepth()+";X");*/
		
		return null;
	}
	
	@Override
	public int sizeBasin(File basin) {
		return 0;
	}
	
	@Override
	public int minDepth(File basin) {
		return 0;
	}
	
	@Override
	public int maxDepth(File basin) {
		return 0;
	}
	
	@Override
	public float avgDepth(File basin) {
		return 0;
	}
	
	@Override
	public void checkAround(Instance tsp, Solution solution, ArrayList<HashFit> basin, ArrayList<HashFit> border, int depth) {
//		System.out.println("----->"+depth);
		int indexBasin,indexBorder,i,j,n = tsp.getDimension();
		Solution neighbour,localOpt;
		HashFit hashFit,ext;
		neighborhood.TwoOpt twoOpt = new neighborhood.TwoOpt();
		localsearch.TwoOpt twoOptLS = new localsearch.TwoOpt();
		for (i = 0; i <= n - 2; i++) {
			for (j = i + 2; (i == 0 && j < n - 1) || (i > 0 && j < n); j++) {
//				System.out.println(i+","+j+" - "+depth);
				neighbour =twoOpt.move(tsp,solution.copy(),new Movement(i,j));
//				System.out.println(solution);
				localOpt = neighbour.copy();
				twoOptLS.bestFit(localOpt, tsp);
//				System.out.println(new HashFit(localOpt.getHash(), localOpt.getFitness())+"\t"+i+"-"+j);
				if (new HashFit(localOpt).equals(basin.get(0))) {
					hashFit = new HashFit(neighbour.getHash(),neighbour.getFitness());
					indexBasin = basin.indexOf(hashFit);
					if(indexBasin<0) {
						basin.add(new HashFit(hashFit.getHash(), hashFit.getFitness(), depth));
//						System.out.println(neighbour);
						checkAround(tsp, neighbour, basin, border, depth+1);
//						System.out.println("<-----"+depth);
					}else if(basin.get(indexBasin).getDepth()>depth){
//						System.out.println("Update depth: from "+basin.get(index).getDepth()+" to "+depth);
						basin.get(indexBasin).setDepth(depth);
					}
				}else{
					ext = new HashFit(neighbour,depth);
					indexBorder = border.indexOf(ext);
					if(indexBorder<0)
						border.add(ext);
					else if(border.get(indexBorder).getDepth()>depth)
						border.get(indexBorder).setDepth(depth);
					//System.out.println("[X]\t"+new HashFit(neighbour, depth));
				}
			}
		}
	}
	
	public ArrayList<HashFit> explore(Instance tsp, Solution localOpt){
		int depth,index,i,j,n = tsp.getDimension();
		Solution neighbour,solution,localOpt2;
		HashFit hashFit;
		neighborhood.TwoOpt twoOpt = new neighborhood.TwoOpt();
		localsearch.TwoOpt twoOptLS = new localsearch.TwoOpt();
		ArrayList<HashFit> basin = new ArrayList<>();
		LinkedList<Solution> queue = new LinkedList<>();
		depth = 0;
		basin.add(new HashFit(localOpt.getHash(),localOpt.getFitness()));
		queue.add(localOpt);
		while (!queue.isEmpty()){
//			System.out.println(queue.size());
			solution = queue.getFirst();
			for (i = 0; i <= n - 2; i++) {
				for (j = i + 2; (i == 0 && j < n - 1) || (i > 0 && j < n); j++) {
					neighbour =twoOpt.move(tsp,solution.copy(),new Movement(i,j));
					localOpt2 = neighbour.copy();
					twoOptLS.bestFit(localOpt2, tsp);
					hashFit = new HashFit(neighbour.getHash(),neighbour.getFitness());
					index = basin.indexOf(hashFit);
					if (index<0 && new HashFit(localOpt2.getHash(), localOpt2.getFitness()).equals(basin.get(0))) {
						queue.add(neighbour);
						basin.add(new HashFit(hashFit.getHash(), hashFit.getFitness(), depth));
//						System.out.println(neighbour);
//						System.out.println("<-----"+depth);
					
					}
					else{
						//System.out.println("*******");
					}
				}
			}
			queue.removeFirst();
		}
		
		return basin;
		
	}
	
	
}
