package generator.set;

import tsp.Solution;

import java.util.ArrayList;
import java.util.Arrays;

import static utils.File.addSolutionToFile;

class Utils {
	static ArrayList<Solution> addIfNotExists(ArrayList<Solution> pop, Solution s, String file){
		for(Solution ss : pop){
			if(ss.equals(s))
				return pop;
		}
		pop.add(s);
		addSolutionToFile(file,s);
		return pop;
	}
}
