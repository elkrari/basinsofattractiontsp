package generator;

import java.util.Random;

public class RandGen {
  
  
  /**
   * Integer generator between a minimum and a maximum
   * @param min Minimal value of the random generated Integer
   * @param max Maximal value of the random generated Integer
   * @return An Integer from min to max
   */
  public static int randInt(int min, int max) {

    Random rand = new Random();
    int randomNum = rand.nextInt((max - min) + 1) + min;

    return randomNum;
  }

}
